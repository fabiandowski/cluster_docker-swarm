# provider "vsphere" {
#   user           = "administrator@vsphere.local"
#   password       = "Zsedcx.qwerty.123"
#   vsphere_server = "vcenter.cloud.local"

#   # If you have a self-signed cert
#   allow_unverified_ssl = true
# }

# data "vsphere_datacenter" "dc" {
#   name = "Escuela"
# }

# data "vsphere_datastore" "datastore" {
#   name          = "DS_104"
#   datacenter_id = data.vsphere_datacenter.dc.id
# }

# data "vsphere_resource_pool" "pool" {
#   name          = "terraform"
#   datacenter_id = data.vsphere_datacenter.dc.id
# }

# data "vsphere_network" "network" {
#   name          = "VLAN_102"
#   datacenter_id = data.vsphere_datacenter.dc.id
# }

# data "vsphere_network" "mgmt_lan" {
#   name          = "VLAN_102"
#   datacenter_id = data.vsphere_datacenter.dc.id
# }

# resource "vsphere_virtual_machine" "vm" {
#   name             = "prueba_automatizacion"
#   resource_pool_id = data.vsphere_resource_pool.pool.id
#   datastore_id     = data.vsphere_datastore.datastore.id

#   num_cpus   = 1
#   memory     = 2048
#   wait_for_guest_net_timeout = 0
#   guest_id = "centos7_64Guest"
#   nested_hv_enabled =true
#   network_interface {
#    network_id     = "${data.vsphere_network.mgmt_lan.id}"
#    guest_ip_addresses= "10.0.0.1"
#    adapter_type   = "vmxnet3"
#   }

#   disk {
#     label = "disk0"
#     size  = 20
#   }
# }

# Basic configuration withour variables

# Define authentification configuration
provider "vsphere" {
  # If you use a domain set your login like this "MyDomain\\MyUser"
  user           = "administrator@vsphere.local"
  password       = "Zsedcx.qwerty.123"
  vsphere_server = "vcenter.cloud.local"

  # if you have a self-signed cert
  allow_unverified_ssl = true
}

#### RETRIEVE DATA INFORMATION ON VCENTER ####

data "vsphere_datacenter" "dc" {
  name = "Escuela"
}

data "vsphere_resource_pool" "pool" {
  # If you haven't resource pool, put "Resources" after cluster name
  name          = "FMENDIETA"
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_host" "host" {
  name          = "172.19.2.104"
  datacenter_id = data.vsphere_datacenter.dc.id
}

# Retrieve datastore information on vsphere
data "vsphere_datastore" "datastore" {
  name          = "DS_104"
  datacenter_id = data.vsphere_datacenter.dc.id
}

# Retrieve network information on vsphere
data "vsphere_network" "network" {
  name          = "VLAN_102"
  datacenter_id = data.vsphere_datacenter.dc.id
}

# Retrieve template information on vsphere
data "vsphere_virtual_machine" "template" {
  name          = "template-kubernetes"
  datacenter_id = data.vsphere_datacenter.dc.id
}

#### VM Docker-Swarm Master ####

# Set vm parameters
resource "vsphere_virtual_machine" "docker-swarm-master" {
  name             = "docker-swarm-master"
  num_cpus         = 2
  memory           = 4096
  datastore_id     = data.vsphere_datastore.datastore.id
  host_system_id   = data.vsphere_host.host.id
  resource_pool_id = data.vsphere_resource_pool.pool.id
  guest_id         = data.vsphere_virtual_machine.template.guest_id
  scsi_type        = data.vsphere_virtual_machine.template.scsi_type

  # Set network parameters
  network_interface {
    network_id = data.vsphere_network.network.id
  }

  # Use a predefined vmware template has main disk
  disk {
    name = "docker-swarm-master.vmdk"
    size = "30"
    thin_provisioned = false
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id

    customize {
      linux_options {
        host_name = "docker-swarm-master"
        domain    = "test.internal"
      }

      network_interface {
        ipv4_address    = "172.19.2.154"
        ipv4_netmask    = 24
        dns_server_list = ["8.8.8.8", "8.8.4.4"]
      }

      ipv4_gateway = "172.19.2.1"
    }
  }
}

### VM Docker-Swarm Worker1 ####

# Set vm parameters
resource "vsphere_virtual_machine" "docker-swarm-worker1" {
  name             = "docker-swarm-worker1"
  num_cpus         = 2
  memory           = 4096
  datastore_id     = data.vsphere_datastore.datastore.id
  host_system_id   = data.vsphere_host.host.id
  resource_pool_id = data.vsphere_resource_pool.pool.id
  guest_id         = data.vsphere_virtual_machine.template.guest_id
  scsi_type        = data.vsphere_virtual_machine.template.scsi_type

  # Set network parameters
  network_interface {
    network_id = data.vsphere_network.network.id
  }

  # Use a predefined vmware template has main disk
  disk {
    name = "docker-swarm-worker1.vmdk"
    size = "30"
    thin_provisioned = false
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id

    customize {
      linux_options {
        host_name = "docker-swarm-worker1"
        domain    = "test.internal"
      }

      network_interface {
        ipv4_address    = "172.19.2.155"
        ipv4_netmask    = 24
        dns_server_list = ["8.8.8.8", "8.8.4.4"]
      }

      ipv4_gateway = "172.19.2.1"
    }
  }
}

### VM Docker-Swarm Worker2 ####

#Set vm parameters
resource "vsphere_virtual_machine" "docker-swarm-worker2" {
  name             = "docker-swarm-worker2"
  num_cpus         = 2
  memory           = 4096
  datastore_id     = data.vsphere_datastore.datastore.id
  host_system_id   = data.vsphere_host.host.id
  resource_pool_id = data.vsphere_resource_pool.pool.id
  guest_id         = data.vsphere_virtual_machine.template.guest_id
  scsi_type        = data.vsphere_virtual_machine.template.scsi_type

  # Set network parameters
  network_interface {
    network_id = data.vsphere_network.network.id
  }

  # Use a predefined vmware template has main disk
  disk {
    name = "docker-swarm-worker2.vmdk"
    size = "30"
    thin_provisioned = false
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id

    customize {
      linux_options {
        host_name = "docker-swarm-worker2"
        domain    = "test.internal"
      }

      network_interface {
        ipv4_address    = "172.19.2.156"
        ipv4_netmask    = 24
        dns_server_list = ["8.8.8.8", "8.8.4.4"]
      }

      ipv4_gateway = "172.19.2.1"
    }
  }
}
